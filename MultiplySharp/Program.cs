﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiplySharp
{
    class Program
    {
        const int ROWS = 8192;
        const int COLS = 8192;
        static void multiply(int[,] matrix, int[] vector, int[] product)
        {
            for (int i = 0; i < ROWS; i++)
            {
                for (int j = 0; j < COLS; j++)
                {
                    product[i] += (matrix[i,j] * vector[j]);
                }
            }
        }

        static void multiply_thread(int[,] matrix, int[] vector, int[] product, int start, int cuts)
        {
            for (int i = start; i < start + ROWS / cuts; i++)
            {
                for (int j = 0; j < COLS; j++)
                {
                    product[i] += (matrix[i,j] * vector[j]);
                }
            }
        }

        static void Main(string[] args)
        {
            int i, j;
            int[,] random_matrix = new int[ROWS,COLS];
            int[] random_vector = new int[ROWS];
            int[] product = new int[COLS];

            Random rnd = new Random();

            for (i = 0; i < ROWS; i++)
                for (j = 0; j < COLS; j++)
                {
                    random_matrix[i,j] = rnd.Next(0,1000);
                }

            for (i = 0; i < ROWS; i++)
                random_vector[i] = rnd.Next(0, 1000);


            Console.WriteLine("Multiplying 8192x8192 random int matrix by 8192 int vector, 1 thread");
            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();
            multiply(random_matrix, random_vector, product);
            stopwatch.Stop();

            Console.WriteLine("Time elapsed: {0} ms", stopwatch.ElapsedMilliseconds);


            Console.WriteLine("Multiplying 8192x8192 random int matrix by 8192 int vector, 2 threads");

            stopwatch.Reset();
            stopwatch.Start();
            Thread thread1 = new Thread(() => multiply_thread(random_matrix, random_vector, product, 0, 2));
            Thread thread2 = new Thread(() => multiply_thread(random_matrix, random_vector, product, 4096, 2));
            thread1.Start();
            thread2.Start();
            thread1.Join();
            thread2.Join();

            stopwatch.Stop();


            Console.WriteLine("Time elapsed: {0} ms", stopwatch.ElapsedMilliseconds);

            Console.WriteLine("Multiplying 8192x8192 random int matrix by 8192 int vector, 4 threads");

            stopwatch.Reset();
            stopwatch.Start();
            Thread thread3 = new Thread(() => multiply_thread(random_matrix, random_vector, product, 0, 4));
            Thread thread4 = new Thread(() => multiply_thread(random_matrix, random_vector, product, 2048, 4));
            Thread thread5 = new Thread(() => multiply_thread(random_matrix, random_vector, product, 4096, 4));
            Thread thread6 = new Thread(() => multiply_thread(random_matrix, random_vector, product, 6144, 4));
            thread3.Start();
            thread4.Start();
            thread5.Start();
            thread6.Start();
            thread3.Join();
            thread4.Join();
            thread5.Join();
            thread6.Join();

            stopwatch.Stop();


            Console.WriteLine("Time elapsed: {0} ms", stopwatch.ElapsedMilliseconds);
        }
    }
}
